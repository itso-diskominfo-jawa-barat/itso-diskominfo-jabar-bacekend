<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\AsetController;
use App\Http\Controllers\AuthController;
use App\Http\Controllers\RoleController;
use App\Http\Controllers\UnitController;
use App\Http\Controllers\UserController;
use App\Http\Controllers\ModuleController;
use App\Http\Controllers\BookingController;
use App\Http\Controllers\ApprovalController;
use App\Http\Controllers\EmployeeController;
use App\Http\Controllers\PositionController;
use App\Http\Controllers\PermissionController;
use App\Http\Controllers\MasterApprovalController;

Route::post('/login', [AuthController::class, 'login']);

Route::middleware('auth:sanctum')->group(function () 
{
    // Role
    Route::apiResource('/roles', RoleController::class);
    // Booking
    Route::apiResource('/bookings', BookingController::class);
    // Module
    Route::apiResource('/modules', ModuleController::class);
    // Employee
    Route::apiResource('/employees', EmployeeController::class)->except('update');
    Route::post('/employees/{id}', [EmployeeController::class, 'update']);
    // Aset
    Route::apiResource('/asets', AsetController::class);
    // User
    Route::apiResource('/users', UserController::class);
    // Unit
    Route::apiResource('/units', UnitController::class);
    // Auth
    Route::get('/me', [AuthController::class, 'me']);
    Route::post('/logout', [AuthController::class, 'logout']);
    // Position
    Route::apiResource('/positions', PositionController::class);
    // Permission
    Route::apiResource('/permissions', PermissionController::class);
    // Master Approval
    Route::apiResource('/master-approval', MasterApprovalController::class);
    // Approval
    Route::apiResource('/approval', ApprovalController::class);
    Route::put('/{module}/{resorceId}/approve', [ApprovalController::class, 'approve']);
    Route::put('/{module}/{resorceId}/ended', [ApprovalController::class, 'ended']);
    Route::put('/reject/{approvalId}', [ApprovalController::class, 'reject']);
});


