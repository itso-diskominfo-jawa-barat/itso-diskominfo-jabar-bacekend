<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::table('User', function (Blueprint $table) {
            $table->foreign(['UserEmployeeId'], 'User_UserEmployeeId_fkey')->references(['EmployeeId'])->on('Employee')->onUpdate('no action')->onDelete('no action');
            $table->foreign(['UserRoleId'], 'User_UserRoleId_fkey')->references(['MasterRoleId'])->on('MasterRole')->onUpdate('no action')->onDelete('no action');
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::table('User', function (Blueprint $table) {
            $table->dropForeign('User_UserEmployeeId_fkey');
            $table->dropForeign('User_UserRoleId_fkey');
        });
    }
};
