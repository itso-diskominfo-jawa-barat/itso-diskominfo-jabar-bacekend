<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::table('Booking', function (Blueprint $table) {
            $table->foreign(['BookingAsetId'], 'Booking_BookingAsetId_fkey')->references(['MasterAsetId'])->on('MasterAset')->onUpdate('no action')->onDelete('no action');
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::table('Booking', function (Blueprint $table) {
            $table->dropForeign('Booking_BookingAsetId_fkey');
        });
    }
};
