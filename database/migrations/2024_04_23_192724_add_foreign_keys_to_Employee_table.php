<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::table('Employee', function (Blueprint $table) {
            $table->foreign(['EmployeePositionId'], 'Employee_EmployeeRoleId_fkey')->references(['MasterPositionId'])->on('MasterPosition')->onUpdate('no action')->onDelete('no action');
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::table('Employee', function (Blueprint $table) {
            $table->dropForeign('Employee_EmployeeRoleId_fkey');
        });
    }
};
