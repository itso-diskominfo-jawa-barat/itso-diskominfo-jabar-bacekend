<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('Approval', function (Blueprint $table) {
            $table->increments('ApprovalId');
            $table->integer('ApprovalModuleId')->nullable();
            $table->integer('ApprovalSourceId')->nullable();
            $table->integer('ApprovalUserId')->nullable();
            $table->smallInteger('ApprovalStatus')->nullable()->comment('0 : WAITING;1: APPROVED; 2 REJECTED');
            $table->text('ApprovalRemark')->nullable();
            $table->timestamp('ApprovalCreatedAt', 6)->nullable();
            $table->timestamp('ApprovalUpdatedAt')->nullable();
            $table->timestamp('ApprovalDeletedAt', 6)->nullable();
            $table->smallInteger('ApprovalCreatedBy')->nullable();
            $table->smallInteger('ApprovalUpdatedBy')->nullable();
            $table->smallInteger('ApprovalDeletedBy')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('Approval');
    }
};
