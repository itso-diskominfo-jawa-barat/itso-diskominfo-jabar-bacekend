<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::table('Permission', function (Blueprint $table) {
            $table->foreign(['PermissionModuleId'], 'Permission_PermissionModuleId_fkey')->references(['MasterModuleId'])->on('MasterModule')->onUpdate('no action')->onDelete('no action');
            $table->foreign(['PermissionRoleId'], 'Permission_PermissionRoleId_fkey')->references(['MasterRoleId'])->on('MasterRole')->onUpdate('no action')->onDelete('no action');
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::table('Permission', function (Blueprint $table) {
            $table->dropForeign('Permission_PermissionModuleId_fkey');
            $table->dropForeign('Permission_PermissionRoleId_fkey');
        });
    }
};
