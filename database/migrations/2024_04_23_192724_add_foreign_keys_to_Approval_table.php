<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::table('Approval', function (Blueprint $table) {
            $table->foreign(['ApprovalModuleId'], 'Approval_ApprovalModuleId_fkey')->references(['MasterModuleId'])->on('MasterModule')->onUpdate('no action')->onDelete('no action');
            $table->foreign(['ApprovalUserId'], 'Approval_ApprovalUserId_fkey')->references(['UserId'])->on('User')->onUpdate('no action')->onDelete('no action');
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::table('Approval', function (Blueprint $table) {
            $table->dropForeign('Approval_ApprovalModuleId_fkey');
            $table->dropForeign('Approval_ApprovalUserId_fkey');
        });
    }
};
