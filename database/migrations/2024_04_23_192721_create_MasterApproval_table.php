<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('MasterApproval', function (Blueprint $table) {
            $table->increments('MasterApprovalId');
            $table->integer('MasterApprovalModuleId')->nullable();
            $table->integer('MasterApprovalPositionId')->nullable();
            $table->timestamp('MasterApprovalCreatedAt', 6)->nullable();
            $table->timestamp('MasterApprovalUpdatedAt')->nullable();
            $table->timestamp('MasterApprovalDeletedAt', 6)->nullable();
            $table->integer('MasterApprovalCreatedBy')->nullable();
            $table->integer('MasterApprovalUpdatedBy')->nullable();
            $table->integer('MasterApprovalDeletedBy')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('MasterApproval');
    }
};
