<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::table('MasterApproval', function (Blueprint $table) {
            $table->foreign(['MasterApprovalPositionId'], 'MasterApproval_MasterApprovalEmployeeId_fkey')->references(['MasterPositionId'])->on('MasterPosition')->onUpdate('no action')->onDelete('no action');
            $table->foreign(['MasterApprovalModuleId'], 'MasterApproval_MasterApprovalModuleId_fkey')->references(['MasterModuleId'])->on('MasterModule')->onUpdate('no action')->onDelete('no action');
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::table('MasterApproval', function (Blueprint $table) {
            $table->dropForeign('MasterApproval_MasterApprovalEmployeeId_fkey');
            $table->dropForeign('MasterApproval_MasterApprovalModuleId_fkey');
        });
    }
};
