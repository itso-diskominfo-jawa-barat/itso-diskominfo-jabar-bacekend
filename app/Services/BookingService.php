<?php

namespace App\Services;

use Carbon\Carbon;
use App\Enums\ModuleEnum;
use App\Enums\BookingEnum;
use App\Helpers\BookingHelper;
use App\Repositories\BookingRepository;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;

class BookingService
{
    protected   $repository, 
                $bookingHelper, 
                $approvalService;

    public function __construct(
        BookingRepository $repository, 
        BookingHelper $bookingHelper,
        ApprovalService $approvalService
    )
    {
        $this->repository = $repository;
        $this->bookingHelper = $bookingHelper;
        $this->approvalService = $approvalService;
    }
    public function store(array $data,$userId)
    {
        $exit = $this->repository;

        if($data['BookingStart'] >= $data['BookingEnd'])
        {
            throw new BadRequestHttpException('Booking Start Date Must Be Greater Than Booking End Date!');
        }

        if($exit->where('BookingAsetId', $data['BookingAsetId'])->where('BookingStatus', BookingEnum::BOOKING)->count())
        {
            throw new BadRequestHttpException('Asset Has Been Booked!');
        }
        
        if ($exit->where('BookingAsetId', $data['BookingAsetId'])
                    ->where('BookingStatus', BookingEnum::WAITING)
                    ->where('BookingExpired', '>=', Carbon::now()->format('Y-m-d H:i:s'))
                    ->exists()) 
        {
            throw new BadRequestHttpException('Asset Has Been Waiting!, please try a few minutes later!');
        }

        $code = $this->bookingHelper->createrandobooking(5);
        $data['BookingStatus'] = 0;
        $data['BookingCode'] = $code;
        $data['BookingExpired'] = Carbon::now()->addMinutes(5)->format('Y-m-d H:i:s');

        $store = $this->repository->create($data);
        
        $this->approvalService->createapproval(ModuleEnum::BOOKING,$store->BookingId,$userId);

        return $store;
    }
}