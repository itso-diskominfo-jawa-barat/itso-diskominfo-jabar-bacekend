<?php
namespace App\Services;

use App\Helpers\AsetHelper;
use App\Repositories\AsetRepository;
class AsetService
{
    protected $repository, 
              $asetHelper;
    public function __construct(AsetRepository $repository, AsetHelper $asetHelper)
    {
        $this->repository = $repository;
        $this->asetHelper = $asetHelper;
    }
    public function store(array $data)
    {
        $asetCodes = $this->asetHelper->generateasetcode($data['MasterAsetBoughtDate'],5);
        $data['MasterAsetCode'] = $asetCodes;

        $aset = $this->repository->create($data);

        return $aset;
    }
}