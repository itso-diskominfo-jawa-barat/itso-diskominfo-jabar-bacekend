<?php

namespace App\Services;
use App\Repositories\UserRepository;
class UserService
{
    protected $userRepository;
    public function __construct(UserRepository $userRepository)
    {
        $this->userRepository = $userRepository;
    }
    public function update($request, $id)
    {
        if ($request->has('UserPassword')) {
            $request->merge(['password' => bcrypt($request->input('UserPassword'))]);
        }
        
        $user = $this->userRepository->update($request->all(), $id);

        return $user;
    }
}