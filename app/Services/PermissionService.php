<?php
namespace App\Services;
use App\Repositories\ModuleRepository;
use App\Repositories\PermissionRepository;
use App\Repositories\RoleRepository;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;

class PermissionService
{
    protected $repository,$moduleRepository,$roleRepository;
    public function __construct(PermissionRepository $repository, ModuleRepository $moduleRepository, RoleRepository $roleRepository)
    {
        $this->repository = $repository;
        $this->moduleRepository = $moduleRepository;
        $this->roleRepository = $roleRepository;
    }
    public function managePermission($module, $role, $master)
    {
        $roles = $this->roleRepository->find($role);
        
        if($roles->MasterRoleName == 'SA')
        {

        }else{

            $fetchModule = $this->moduleRepository->where('MasterModuleCode', $module)->first();
            $permission = $this->repository->where('PermissionModuleId', $fetchModule->MasterModuleId)->where('PermissionRoleId', $role)->where('PermissionMaster', $master)->first();
            
            if (!$permission) {
                throw new BadRequestHttpException('You do not have permission to access this resource!, please call administrator!');
            }
        }
        return true;
    }
}