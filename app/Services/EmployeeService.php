<?php

namespace App\Services;

use App\Enums\ResourceEnum;
use App\Helpers\EmployeeHelper;
use App\Helpers\StorageHelper;
use App\Repositories\UserRepository;
use App\Repositories\EmployeeRepository;

class EmployeeService
{
    protected $employeeRepository, $userRepository, $employeeHelper;
    public function __construct(EmployeeRepository $employeeRepository, UserRepository $userRepository, EmployeeHelper $employeeHelper)
    {
        $this->employeeRepository = $employeeRepository;
        $this->userRepository = $userRepository;
        $this->employeeHelper = $employeeHelper;
    }

    public function store(array $data)
    {
        $employeeHelper = $this->employeeHelper->generateNumber(5);
        
        $dataEmployee = $this->employeeRepository->create([
            'EmployeeName' => $data['EmployeeName'],
            'EmployeeNumber' => $employeeHelper,
            'EmployeeEmail' => $data['EmployeeEmail'],
            'EmployeePhone' => $data['EmployeePhone'],
            'EmployeePositionId' => $data['EmployeePositionId']
        ]);

        $dataUser = $this->userRepository->create([
            'UserEmployeeId' => $dataEmployee['EmployeeId'],
            'name' => $this->employeeHelper->generateusername(10),
            'password' => bcrypt($this->employeeHelper->createPassword())
        ]);
        
        return [
            'dataEmployee' => $dataEmployee,
            'dataUser' => $dataUser
        ];
    }

    public function update($request , $image, int $id)
    {
        if($image)
        {
           $path = StorageHelper::storeFileImage($image, ResourceEnum::EMPLOYEE);
        }
        
        $employe = $this->employeeRepository->update([
            $request,
            'EmployeeImage' => $path
        ], $id);

        return $employe;

    }
}