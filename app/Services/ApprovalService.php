<?php
namespace App\Services;
use App\Enums\ApprovalEnum;
use App\Repositories\ModuleRepository;
use App\Repositories\ApprovalRepository;
use App\Repositories\MasterApprovalRepository;
use Exception;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;

class ApprovalService
{
    protected $masterModule, $repository, $masterApprovalRepository;
    public function __construct(ModuleRepository $masterModule, ApprovalRepository $repository, MasterApprovalRepository $masterApprovalRepository)
    {
        $this->masterModule = $masterModule;
        $this->repository = $repository;
        $this->masterApprovalRepository = $masterApprovalRepository;
    }
    public function createapproval($moduleName,$resorceId,$userId)
    {
        $moduleId = $this->masterModule->where('MasterModuleCode', $moduleName)->first()->MasterModuleId;

        if(!$moduleId){
            throw new BadRequestHttpException('Module '.$moduleName.' Not Found!');
        }
        
        $masterApproval = $this->masterApprovalRepository->where('MasterApprovalModuleId',$moduleId)->get();

        if(!$masterApproval){
            throw new BadRequestHttpException('Module '.$moduleName.' not found, please call administrator!');
        }

        foreach($masterApproval as $masterApprovals)
        {
            $getPosition = $masterApprovals->position;

            foreach($getPosition as $getPositions)
            {
                $getEmployee = $getPositions->getemployee;

                foreach($getEmployee as $getEmployees)
                {
                    $data = [
                        'ApprovalModuleId' => $moduleId,
                        'ApprovalSourceId' => $resorceId,
                        'ApprovalUserId' => $getEmployees->user->UserId,
                        'ApprovalCreatedBy' => $userId,
                        'ApprovalUpdatedBy' => $userId
                    ];
                    
                    $approval = $this->repository->create($data);
                }    
            }
        }
        return $approval;
    }

    public function approve($module, $resorceId, $data, $userId)
    {
        $module = $this->masterModule->where('MasterModuleType', $module)->first()->MasterModuleId;

        $approval = $this->repository
                    ->where('ApprovalModuleId', $module)
                    ->where('ApprovalSourceId', $resorceId)
                    ->get();
        
        $approvalId = $approval->pluck('ApprovalUserId')->toArray();
        
        if(!$approval){
            throw new Exception('Approval Not Found!');
        }
        
        $matchingIds = array_intersect($approvalId, $userId);

        if (count($matchingIds) < 0) {
            throw new Exception('You not allowed to approve this data!');
        }

        foreach($approval as $approvals)
        {
            if($approvals->ApprovalStatus == ApprovalEnum::REJECTED)
            {
                throw new Exception('Approval Already Rejected!');
            }

            if($approvals->ApprovalStatus == ApprovalEnum::APPROVED){
                throw new Exception('Approval Already Approved!');
            }

            $approvals->update([
                'ApprovalStatus' => ApprovalEnum::APPROVED,
                'ApprovalRemark' => $data['Reason'] ?? null
            ]);
        }

        return'This data has been approved!';
    }

    public function reject($approvalId,$userId,$data)
    {
        $approval = $this->repository->find($approvalId);
        
        if($approval->ApprovalStatus == ApprovalEnum::REJECTED)
        {
            throw new Exception('Approval Already Rejected!');
        }

        $approval->update([
            'ApprovalStatus' => ApprovalEnum::REJECTED,
            'ApprovalRemark' => $data['Reason'] ?? null
        ]);

        return'This data has been rejected!';
    }

    public function ended($module, $resorceId, $data, $userId)
    {
        $module = $this->masterModule->where('MasterModuleType', $module)->first()->MasterModuleId;

        $approval = $this->repository
                    ->where('ApprovalModuleId', $module)
                    ->where('ApprovalSourceId', $resorceId)
                    ->where('ApprovalUserId', $userId)
                    ->get();    

        if ($approval->isEmpty()) {
            throw new Exception('Approval Not Found!');
        }

        foreach($approval as $approvals){

            if($approvals->ApprovalStatus == ApprovalEnum::APPROVED)
            {
                $approvals->update([
                    'ApprovalStatus' => ApprovalEnum::ENDED,
                    'ApprovalRemark' => $data['Reason'] ?? 'Success Ended!'
                ]);

            }elseif($approvals->ApprovalStatus == ApprovalEnum::ENDED)
            {
                throw new Exception('Approval Already Ended!');
            }else{
                throw new Exception('Approval Already Rejected!');
            }
        }

        return'This data has been ended!';
    }
}