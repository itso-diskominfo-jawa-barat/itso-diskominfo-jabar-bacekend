<?php
    
namespace App\Repositories;
    
use App\Models\Position;
use Prettus\Repository\Eloquent\BaseRepository;
use Prettus\Repository\Criteria\RequestCriteria;
    
class PositionRepository extends BaseRepository
{
    /**
     * @return string
     */
    
    public function model()
    {
        return Position::class;
    }
    
    /**
     * @throws \Prettus\Repository\Exceptions\RepositoryException
     */

    public function boot()
    {
        $this->pushCriteria(app(RequestCriteria::class));
    }
}
    