<?php
    
namespace App\Repositories;
    
use App\Models\Employee;
use Prettus\Repository\Eloquent\BaseRepository;
use Prettus\Repository\Criteria\RequestCriteria;
    
class EmployeeRepository extends BaseRepository
{
    /**
     * @return string
     */
    
    public function model()
    {
        return Employee::class;
    }

    /**
     * @throws \Prettus\Repository\Exceptions\RepositoryException
     */

    public function boot()
    {
        $this->pushCriteria(app(RequestCriteria::class));
    }
}
    