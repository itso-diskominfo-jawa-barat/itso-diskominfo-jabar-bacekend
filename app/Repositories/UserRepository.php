<?php
    
namespace App\Repositories;
    
use App\Models\User;
use Prettus\Repository\Eloquent\BaseRepository;
use Prettus\Repository\Criteria\RequestCriteria;
    
class UserRepository extends BaseRepository
{
    /**
     * @return string
     */
    
    public function model()
    {
        return User::class;
    }

    /**
     * @throws \Prettus\Repository\Exceptions\RepositoryException
     */

    public function boot()
    {
        $this->pushCriteria(app(RequestCriteria::class));
    }
}
    