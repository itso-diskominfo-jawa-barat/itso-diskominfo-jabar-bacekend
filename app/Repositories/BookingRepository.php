<?php
    
namespace App\Repositories;
    
use App\Models\Booking;
use Prettus\Repository\Eloquent\BaseRepository;
use Prettus\Repository\Criteria\RequestCriteria;
    
class BookingRepository extends BaseRepository
{
    /**
     * @return string
     */
    
    public function model()
    {
        return Booking::class;
    }

    /**
     * @throws \Prettus\Repository\Exceptions\RepositoryException
     */

    public function boot()
    {
        $this->pushCriteria(app(RequestCriteria::class));
    }
}
    