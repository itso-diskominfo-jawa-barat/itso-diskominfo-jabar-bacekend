<?php
    
namespace App\Repositories;
    
use App\Models\Aset;
use Prettus\Repository\Eloquent\BaseRepository;
use Prettus\Repository\Criteria\RequestCriteria;
    
class AsetRepository extends BaseRepository
{
    /**
     * @return string
     */
    
    public function model()
    {
        return Aset::class;
    }
    
    /**
     * @throws \Prettus\Repository\Exceptions\RepositoryException
     */

    public function boot()
    {
        $this->pushCriteria(app(RequestCriteria::class));
    }
}
    