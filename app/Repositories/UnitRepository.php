<?php
    
namespace App\Repositories;
    
use App\Models\Unit;
use Prettus\Repository\Eloquent\BaseRepository;
use Prettus\Repository\Criteria\RequestCriteria;

class UnitRepository extends BaseRepository
{
    /**
     * @return string
     */
    
    public function model()
    {
        return Unit::class;
    }
    
    /**
     * @throws \Prettus\Repository\Exceptions\RepositoryException
     */

    public function boot()
    {
        $this->pushCriteria(app(RequestCriteria::class));
    }
}
    