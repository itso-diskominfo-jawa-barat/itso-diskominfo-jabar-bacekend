<?php
    
namespace App\Repositories;

use App\Models\Approval;
use Prettus\Repository\Eloquent\BaseRepository;
use Prettus\Repository\Criteria\RequestCriteria;
    
class ApprovalRepository extends BaseRepository
{
    /**
     * @return string
     */
    
    public function model()
    {
        return Approval::class;
    }
    
    /**
     * @throws \Prettus\Repository\Exceptions\RepositoryException
     */

    public function boot()
    {
        $this->pushCriteria(app(RequestCriteria::class));
    }
}
    