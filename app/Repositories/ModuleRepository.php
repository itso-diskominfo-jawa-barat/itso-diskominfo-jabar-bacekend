<?php
    
namespace App\Repositories;
    
use App\Models\Module;
use Prettus\Repository\Eloquent\BaseRepository;
use Prettus\Repository\Criteria\RequestCriteria;

class ModuleRepository extends BaseRepository
{
    /**
     * @return string
     */
    public function model()
    {
        return Module::class;
    }
    /**
     * @throws \Prettus\Repository\Exceptions\RepositoryException
     */
    public function boot()
    {
        $this->pushCriteria(app(RequestCriteria::class));
    }
}
    