<?php
    
namespace App\Repositories;
    
use App\Models\MasterApproval;
use Prettus\Repository\Eloquent\BaseRepository;
use Prettus\Repository\Criteria\RequestCriteria;
    
class MasterApprovalRepository extends BaseRepository
{
    /**
     * @return string
     */
    
    public function model()
    {
        return MasterApproval::class;
    }

    /**
     * @throws \Prettus\Repository\Exceptions\RepositoryException
     */

    public function boot()
    {
        $this->pushCriteria(app(RequestCriteria::class));
    }
}
    