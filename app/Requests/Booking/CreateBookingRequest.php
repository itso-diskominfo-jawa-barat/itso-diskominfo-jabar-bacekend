<?php

namespace App\Requests\Booking;
use Illuminate\Foundation\Http\FormRequest;

class CreateBookingRequest extends FormRequest
{
    public function authorize(): bool
    {
        return true;
    }
    public function rules():array
    {
        return [
            'BookingAsetId' => 'required',
            'BookingStart' => 'required',
            'BookingEnd' => 'required',
        ];
    }

    public function prepareForValidation()
    {
        if(auth()->check())
        {
            $this->merge([
                'BookingCreatedBy' => auth()->user()->UserId,
                'BookingUpdatedBy' => auth()->user()->UserId
            ]);
        }
    }
}