<?php

namespace App\Requests\Booking;
use Illuminate\Foundation\Http\FormRequest;

class UpdateBookingRequest extends FormRequest
{
    public function authorize(): bool
    {
        return true;
    }

    public function rules():array
    {
        return [

        ];
    }
    
    public function prepareForValidation()
    {
        if(auth()->check())
        {
            $this->merge([
                'BookingUpdatedBy' => auth()->user()->UserId
            ]);
        }
    }
}