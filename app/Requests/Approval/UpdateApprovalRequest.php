<?php

namespace App\Requests\Approval;
use Illuminate\Foundation\Http\FormRequest;

class UpadateApprovalRequest extends FormRequest
{
    public function authorize(): bool
    {
        return true;
    }

    public function rules():array
    {
        return [
            'MasterAsetName' => 'required',
            'MasterAsetCode' => 'required',
            'MasterAsetType' => 'required',
            'MasterAsetBoughtDate' => 'required'
        ];
    }

    public function prepareForValidation()
    {
        if(auth()->check())
        {
            $this->merge([
                'MasterAsetCreatedBy' => auth()->user()->UserId,
                'MasterAsetUpdatedBy' => auth()->user()->UserId
            ]);
        }
    }
}