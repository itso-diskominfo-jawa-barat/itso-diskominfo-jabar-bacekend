<?php

namespace App\Requests\Module;
use Illuminate\Foundation\Http\FormRequest;

class UpdateModuleRequest extends FormRequest
{
    public function authorize(): bool
    {
        return true;
    }

    public function rules():array
    {
        return [
            'MasterModuleName' => 'unique:MasterModule',
            'MasterModuleCode' => 'unique:MasterModule',
        ];
    }

    public function prepareForValidation()
    {
        if(auth()->check())
        {
            $this->merge([
                'MasterRoleUpdatedBy' => auth()->user()->UserId
            ]);
        }
    }
}