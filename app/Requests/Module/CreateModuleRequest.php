<?php

namespace App\Requests\Module;
use Illuminate\Foundation\Http\FormRequest;

class CreateModuleRequest extends FormRequest
{
    public function authorize(): bool
    {
        return true;
    }

    public function rules():array
    {
        return [
            'MasterModuleName' => 'required|unique:MasterModule',
            'MasterModuleCode' => 'required|unique:MasterModule',
        ];
    }

    public function prepareForValidation()
    {
        if(auth()->check())
        {
            $this->merge([
                'MasterModuleCreatedBy' => auth()->user()->UserId,
                'MasterModuleUpdatedBy' => auth()->user()->UserId
            ]);
        }
    }
}