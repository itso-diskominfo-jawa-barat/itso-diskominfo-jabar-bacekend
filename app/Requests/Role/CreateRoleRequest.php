<?php

namespace App\Requests\Role;
use Illuminate\Foundation\Http\FormRequest;

class CreateRoleRequest extends FormRequest
{
    public function authorize(): bool
    {
        return true;
    }

    public function rules():array
    {
        return [
            'MasterRoleName' => 'required|unique:MasterRole',
            'MasterRoleCode' => 'required|unique:MasterRole'
        ];
    }
    
    public function prepareForValidation()
    {
        if(auth()->check())
        {
            $this->merge([
                'MasterRoleCreatedBy' => auth()->user()->UserId,
                'MasterRoleUpdatedBy' => auth()->user()->UserId
            ]);
        }
    }
}