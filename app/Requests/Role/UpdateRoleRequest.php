<?php

namespace App\Requests\Role;
use Illuminate\Foundation\Http\FormRequest;

class UpdateRoleRequest extends FormRequest
{
    public function authorize(): bool
    {
        return true;
    }

    public function rules():array
    {
        return [
            
        ];
    }
    
    public function prepareForValidation()
    {
        if(auth()->check())
        {
            $this->merge([
                'MasterRoleUpdatedBy' => auth()->user()->UserId
            ]);
        }
    }
}