<?php

namespace App\Requests\Permission;
use Illuminate\Foundation\Http\FormRequest;

class UpdatePermissionRequest extends FormRequest
{
    public function authorize(): bool
    {
        return true;
    }

    public function rules():array
    {
        return [
            
        ];
    }

    public function prepareForValidation()
    {
        if(auth()->check())
        {
            $this->merge([
                'PermissionUpdatedBy' => auth()->user()->UserId
            ]);
        }
    }
}