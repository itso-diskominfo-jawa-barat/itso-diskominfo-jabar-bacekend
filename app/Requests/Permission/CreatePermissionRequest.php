<?php

namespace App\Requests\Permission;
use Illuminate\Foundation\Http\FormRequest;

class CreatePermissionRequest extends FormRequest
{
    public function authorize(): bool
    {
        return true;
    }

    public function rules():array
    {
        return [
            'PermissionModuleId' => 'required',
            'PermissionRoleId' => 'required',
        ];
    }

    public function prepareForValidation()
    {
        if(auth()->check())
        {
            $this->merge([
                'PermissionCreatedBy' => auth()->user()->UserId,
                'PermissionUpdatedBy' => auth()->user()->UserId
            ]);
        }
    }
}