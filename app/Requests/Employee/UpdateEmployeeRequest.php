<?php

namespace App\Requests\Employee;
use Illuminate\Foundation\Http\FormRequest;

class UpdateEmployeeRequest extends FormRequest
{
    public function authorize(): bool
    {
        return true;
    }
    
    public function prepareForValidation()
    {
        if(auth()->check())
        {
            $this->merge([
                'EmployeeUpdatedBy' => auth()->user()->UserId
            ]);
        }
    }
}