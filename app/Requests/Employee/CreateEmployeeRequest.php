<?php

namespace App\Requests\Employee;
use Illuminate\Foundation\Http\FormRequest;

class CreateEmployeeRequest extends FormRequest
{
    public function authorize(): bool
    {
        return true;
    }

    public function rules():array
    {
        return [
            'EmployeeName' => 'required',
            'EmployeePositionId' => 'required|exists:MasterPosition,MasterPositionId',
            'EmployeeGender' => 'required',
            'EmployeeAddress' => 'required',
            'EmployeeEmail' => 'required|email|unique:Employee',
            'EmployeePhone' => 'required|unique:Employee'
        ];
    }

    public function prepareForValidation()
    {
        if(auth()->check())
        {
            $this->merge([
                'EmployeeCreatedBy' => auth()->user()->UserId,
                'EmployeeUpdatedBy' => auth()->user()->UserId
            ]);
        }
    }
}