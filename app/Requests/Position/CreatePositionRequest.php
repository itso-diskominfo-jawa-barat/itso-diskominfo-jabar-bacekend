<?php

namespace App\Requests\Position;
use Illuminate\Foundation\Http\FormRequest;

class CreatePositionRequest extends FormRequest
{
    public function authorize(): bool
    {
        return true;
    }

    public function rules():array
    {
        return [
            'MasterPositionName' => 'required|unique:MasterPosition',
            'MasterPositionCode' => 'required|unique:MasterPosition',
            'MasterPositionUnitId' => 'required',
        ];
    }

    public function prepareForValidation()
    {
        if(auth()->check())
        {
            $this->merge([
                'MasterPositionCreatedBy' => auth()->user()->UserId,
                'MasterPositionUpdatedBy' => auth()->user()->UserId
            ]);
        }
    }
}