<?php

namespace App\Requests\Position;
use Illuminate\Foundation\Http\FormRequest;

class UpdatePositionRequest extends FormRequest
{
    public function authorize(): bool
    {
        return true;
    }

    public function rules():array
    {
        return [
          
        ];
    }
    
    public function prepareForValidation()
    {
        if(auth()->check())
        {
            $this->merge([
                'MasterPositionUpdatedBy' => auth()->user()->UserId
            ]);
        }
    }
}