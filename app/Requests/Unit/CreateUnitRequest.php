<?php

namespace App\Requests\Unit;
use Illuminate\Foundation\Http\FormRequest;

class CreateUnitRequest extends FormRequest
{
    public function authorize(): bool
    {
        return true;
    }

    public function rules():array
    {
        return [
            'MasterUnitName' => 'required|unique:MasterUnit',
            'MasterUnitCode' => 'required|unique:MasterUnit',
            'MasterUnitInitial' => 'required|unique:MasterUnit',
        ];
    }
    
    public function prepareForValidation()
    {
        if(auth()->check())
        {
            $this->merge([
                'MasterUnitCreatedBy' => auth()->user()->UserId,
                'MasterUnitUpdatedBy' => auth()->user()->UserId
            ]);
        }
    }
}