<?php

namespace App\Requests\MasterApproval;
use Illuminate\Foundation\Http\FormRequest;

class CreateMasterApprovalRequest extends FormRequest
{
    public function authorize(): bool
    {
        return true;
    }
    public function rules():array
    {
        return [
            'MasterApprovalModuleId' => 'required|exists:MasterModule,MasterModuleId',
            'MasterApprovalPositionId' => 'required|exists:MasterPosition,MasterPositionId',
            'MasterApprovalStage' => 'required',
        ];
    }
    public function prepareForValidation()
    {
        if(auth()->check())
        {
            $this->merge([
                'MasterApprovalCreatedBy' => auth()->user()->UserId,
                'MasterApprovalUpdatedBy' => auth()->user()->UserId
            ]);
        }
    }
}