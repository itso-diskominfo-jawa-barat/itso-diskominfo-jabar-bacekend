<?php

namespace App\Helpers;

use App\Helpers\GeneralHelper;

class EmployeeHelper extends GeneralHelper
{
    public function generateNumber(int $length)
    {
        $generalHelper = $this->generateRandomNumber($length);
        $year = date('Y');

        $random = $year . $generalHelper;
        return $random;
    }

    public function generateusername(int $length)
    {
        $generalHelper = $this->generateRandomUsername($length);
        return $generalHelper;
    }

    public function createPassword()
    {
        $password = 123456;
        return $password;
    }
}