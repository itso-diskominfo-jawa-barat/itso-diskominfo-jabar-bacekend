<?php

namespace App\Enums;

class MasterEnum
{
    const INDEX = 'index';
    const SHOW = 'show';
    const STORE = 'store';
    const UPDATE = 'update';
    const DELETE = 'delete';
}