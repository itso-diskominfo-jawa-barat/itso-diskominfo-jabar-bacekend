<?php

namespace App\Enums;

class ResourceEnum
{
    const BOOKING = 'bookings';
    const EMPLOYEE = 'employees';
    const ASET = 'asets';
    const USER = 'users';
    const UNIT = 'units';
    const POSITION = 'positions';
    const ROLE = 'roles';
    const MODULE = 'modules';
    const PERMISSION = 'permissions';
    const MASTER_APPROVAL = 'master-approvals';
}