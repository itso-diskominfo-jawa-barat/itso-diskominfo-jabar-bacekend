<?php

namespace App\Enums;

class BookingEnum
{
    const
        BOOKING = 1,
        WAITING = 0,
        REJECT = 3;
}