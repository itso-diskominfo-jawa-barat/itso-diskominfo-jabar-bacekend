<?php

namespace App\Enums;

class ApprovalEnum
{
    const
        WAITING = 0,
        APPROVED = 1,
        REJECTED = 2,
        ENDED = 3;
}