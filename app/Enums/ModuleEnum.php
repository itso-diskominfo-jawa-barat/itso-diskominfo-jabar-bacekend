<?php

namespace App\Enums;

class ModuleEnum
{
    const ASET = 'AS';
    const EMPLOYEE = 'EM';
    const UNIT = 'UN';
    const BOOKING = 'BK';
    const ROLE = 'RL';
    const POSITION = 'PS';
    const MASTERAPPROVAL = 'MA';
    const MODULE = 'MD';
    const PERMISSION = 'PM';
    const USER = 'US';
}