<?php

namespace App\Http\Controllers;

use App\Enums\MasterEnum;
use App\Enums\ModuleEnum;
use App\Services\BookingService;
use App\Services\PermissionService;
use Illuminate\Support\Facades\Auth;
use App\Repositories\BookingRepository;
use App\Requests\Booking\CreateBookingRequest;
use App\Requests\Booking\UpdateBookingRequest;
use Illuminate\Support\Facades\DB;

class BookingController extends Controller
{

    protected $repository, $service, $permissionService;
    public function __construct(BookingRepository $repository, BookingService $service, PermissionService $permissionService)
    {
        $this->repository = $repository;
        $this->service = $service;
        $this->permissionService = $permissionService;
    }
    public function index()
    {
        $limit = request('limit', 10);

        $this->permissionService->managePermission(ModuleEnum::BOOKING, Auth::user()->UserRoleId, MasterEnum::INDEX);
        
        try{
            $booking = $this->repository->with('approval')->orderBy('BookingCreatedAt','desc')->paginate($limit);
        }catch(\Exception $e){
            return $this->errorResponse($e->getMessage(), $e);
        }
        return $this->successResponse($booking);
    }

    public function show(int $id)
    {
        $this->permissionService->managePermission(ModuleEnum::BOOKING, Auth::user()->UserRoleId, MasterEnum::SHOW);

        try{
            $booking = $this->repository->find($id);
        }catch(\Exception $e){
            return $this->errorResponse($e->getMessage(), $e);
        }
        return $this->successResponse($booking);
    }

    public function store(CreateBookingRequest $request)
    {
        DB::beginTransaction();

        $this->permissionService->managePermission(ModuleEnum::BOOKING, Auth::user()->UserRoleId, MasterEnum::STORE);

        try{
            $userId = Auth::id();
            $booking = $this->service->store($request->all(),$userId);
        }catch(\Exception $e){
            DB::rollBack();
            return $this->errorResponse($e->getMessage(), $e);
        }
        
        DB::commit();
        return $this->successResponse($booking);
    }

    public function update(UpdateBookingRequest $request, int $id)
    {
        $this->permissionService->managePermission(ModuleEnum::BOOKING, Auth::user()->UserRoleId, MasterEnum::UPDATE);

        try{
            $booking = $this->repository->update($request->all(), $id);
        }catch(\Exception $e){
            return $this->errorResponse($e->getMessage(), $e);
        }
        return $this->successResponse($booking);
    }
    
    public function destroy(int $id)
    {
        $this->permissionService->managePermission(ModuleEnum::BOOKING, Auth::user()->UserRoleId, MasterEnum::DELETE);

        try{
            $booking = $this->repository->delete($id);
        }catch(\Exception $e){
            return $this->errorResponse($e->getMessage(), $e);
        }
        return $this->successResponse($booking);
    }
}
