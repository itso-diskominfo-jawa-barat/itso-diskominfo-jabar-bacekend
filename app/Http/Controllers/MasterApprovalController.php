<?php

namespace App\Http\Controllers;
use App\Enums\MasterEnum;
use App\Enums\ModuleEnum;
use App\Services\PermissionService;
use Illuminate\Support\Facades\Auth;
use App\Repositories\MasterApprovalRepository;
use App\Requests\Employee\UpdateEmployeeRequest;
use App\Requests\MasterApproval\CreateMasterApprovalRequest;

class MasterApprovalController extends Controller
{
    protected $repository, $service, $permissionService;
    public function __construct(
        MasterApprovalRepository $repository,
        PermissionService $permissionService,
    )   
    {
        $this->repository = $repository;
        $this->permissionService = $permissionService;
    }
    public function index()
    {
        $limit = request('limit', 10);

        $this->permissionService->managePermission(ModuleEnum::MASTERAPPROVAL, Auth::user()->UserRoleId, MasterEnum::INDEX);
        
        try {
            $employee = $this->repository->paginate($limit);
        } catch (\Exception $e) {
            return $this->errorResponse($e->getMessage(), $e);
        }
        return $this->successResponse($employee);
    }
    public function show(int $id)
    {
        $this->permissionService->managePermission(ModuleEnum::MASTERAPPROVAL, Auth::user()->UserRoleId, MasterEnum::SHOW);

        try{
            $employee = $this->repository->find($id);
        }catch(\Exception $e){
            return $this->errorResponse($e->getMessage(), $e);
        }
        return $this->successResponse($employee);
    }
    public function store(CreateMasterApprovalRequest $request)
    {
        $this->permissionService->managePermission(ModuleEnum::MASTERAPPROVAL, Auth::user()->UserRoleId, MasterEnum::STORE);

        try{
            $employee = $this->repository->create($request->all());
        }catch(\Exception $e){
            return $this->errorResponse($e->getMessage(), $e);
        }
        return $this->successResponse($employee);
    }

    public function update(UpdateEmployeeRequest $request, int $id)
    {
        $this->permissionService->managePermission(ModuleEnum::MASTERAPPROVAL, Auth::user()->UserRoleId, MasterEnum::UPDATE);

        try{
            $employee = $this->repository->update($request->all(), $id);
        }catch(\Exception $e){
            return $this->errorResponse($e->getMessage(), $e);
        }
        return $this->successResponse($employee);
    }

    public function destroy(int $id)
    {
        $this->permissionService->managePermission(ModuleEnum::MASTERAPPROVAL, Auth::user()->UserRoleId, MasterEnum::DELETE);

        try{
            $employee = $this->repository->delete($id);
        }catch(\Exception $e){
            return $this->errorResponse($e->getMessage(), $e);
        }
        return $this->successResponse($employee);
    }
}
