<?php

namespace App\Http\Controllers;
use App\Services\ApprovalService;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class ApprovalController extends Controller
{
    protected $repository, $service;    
    public function __construct(ApprovalService $service)
    {
        $this->service = $service;
    }
    public function index()
    {
        try{
            $approval = $this->repository->paginate();
        }catch(\Exception $e){
            return $this->errorResponse($e->getMessage());
        }
        return $this->successResponse($approval);
    }
    public function approve($module, $resorceId,Request $request)
    {
        try{
            $user =[Auth::id()];

            $approval = $this->service->approve($module, $resorceId,$request->all(),$user);
        }catch(\Exception $e){
            return $this->errorResponse($e->getMessage());
        }
        return $this->successResponse($approval);
    }

    public function ended($module, $resorceId,Request $request)
    {
        try{
            $user = Auth::id();

            $approval = $this->service->ended($module, $resorceId,$request->all(),$user);
        }catch(\Exception $e){
            return $this->errorResponse($e->getMessage());
        }
        return $this->successResponse($approval);
    }
    public function reject($approvalId,Request $request)
    {
        try{

            $userId = Auth::id();
            $approval = $this->service->reject($approvalId,$userId, $request->all());
            
        }catch(\Exception $e){
            return $this->errorResponse($e->getMessage());
        }
        return $this->successResponse($approval);
    }
}
