<?php

namespace App\Http\Controllers;

use App\Enums\MasterEnum;
use App\Enums\ModuleEnum;
use Illuminate\Http\Request;
use App\Services\UserService;
use App\Services\PermissionService;
use App\Repositories\UserRepository;
use Illuminate\Support\Facades\Auth;
use App\Requests\User\CreateUserRequest;

class UserController extends Controller
{
    protected   $repository, 
                $permissionService,
                $userService;
                
    public function __construct(UserRepository $repository, PermissionService $permissionService, UserService $userService)
    {
        $this->repository = $repository;
        $this->permissionService = $permissionService;
        $this->userService = $userService;
    }
    public function index()
    {
        $limit = request('limit', 10);
        $this->permissionService->managePermission(ModuleEnum::USER, Auth::user()->UserRoleId, MasterEnum::INDEX);

        try{
            $user = $this->repository->paginate($limit);
        }catch(\Exception $e){
            return $this->errorResponse($e->getMessage(), $e);
        }
        return $this->successResponse($user);
    }

    public function show(int $id)
    {
        $this->permissionService->managePermission(ModuleEnum::USER, Auth::user()->UserRoleId, MasterEnum::SHOW);

        try{
            $user = $this->repository->find($id);
        }catch(\Exception $e){
            return $this->errorResponse($e->getMessage(), $e);
        }
        return $this->successResponse($user);
    }
    public function store(CreateUserRequest $request)
    {
        $this->permissionService->managePermission(ModuleEnum::USER, Auth::user()->UserRoleId, MasterEnum::STORE);
        
        try{
            $user = $this->repository->create([
                'name' => $request->name,
                'password' => bcrypt($request->password),
            ]);
        }catch(\Exception $e){
            return $this->errorResponse($e->getMessage(), $e);
        }
        return $this->successResponse($user);
    }
    
    public function update(Request $request, $id)
    {
        $this->permissionService->managePermission(ModuleEnum::USER, Auth::user()->UserRoleId, MasterEnum::UPDATE);
        
        try {

            $user = $this->userService->update($request, $id);

        } catch (\Exception $e) {
            return $this->errorResponse($e->getMessage(), $e);
        }
        return $this->successResponse($user);
    }

    public function destroy(int $id)
    {
        $this->permissionService->managePermission(ModuleEnum::USER, Auth::user()->UserRoleId, MasterEnum::DELETE);

        try{
            $user = $this->repository->delete($id);
        }catch(\Exception $e){
            return $this->errorResponse($e->getMessage(), $e);
        }
        return $this->successResponse($user);
    }
}
