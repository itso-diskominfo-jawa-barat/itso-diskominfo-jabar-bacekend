<?php

namespace App\Http\Controllers;
use App\Enums\MasterEnum;
use App\Enums\ModuleEnum;
use App\Services\PermissionService;
use App\Repositories\AsetRepository;
use Illuminate\Support\Facades\Auth;
use App\Requests\Aset\CreateAsetRequest;
use App\Requests\Aset\UpdateAsetRequest;
use App\Services\AsetService;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;

class AsetController extends Controller
{
    protected 
        $repository, 
        $permissionService, 
        $asetService;
    public function __construct(
        AsetRepository $repository,
        PermissionService $permissionService,
        AsetService $asetService
    )
    {
        $this->repository = $repository;
        $this->permissionService = $permissionService;
        $this->asetService = $asetService;
    }
    public function index()
    {
        $limit = request('limit', 10);

        $this->permissionService->managePermission(ModuleEnum::ASET, Auth::user()->UserRoleId, MasterEnum::INDEX);
        
        try {
            $aset = $this->repository->paginate($limit);
        } catch (BadRequestHttpException $e) {
            return $this->errorResponse($e->getMessage(), $e);
        }
        return $this->successResponse($aset);
    }
    public function show(int $id)
    {
        $this->permissionService->managePermission(ModuleEnum::ASET, Auth::user()->UserRoleId, MasterEnum::SHOW);

        try{
            $aset = $this->repository->find($id);
        }catch(\Exception $e){
            return $this->errorResponse($e->getMessage());
       }
        return $this->successResponse($aset);
    }
    
    public function store(CreateAsetRequest $request)
    {
        $this->permissionService->managePermission(ModuleEnum::ASET, Auth::user()->UserRoleId, MasterEnum::STORE);

        try{
            $aset = $this->asetService->store($request->all());
        }catch(\Exception $e){
            return $this->errorResponse($e->getMessage());
        }

        return $this->successResponse($aset);
    }
    
    public function update(UpdateAsetRequest $request, $id)
    {
        $this->permissionService->managePermission(ModuleEnum::ASET, Auth::user()->UserRoleId, MasterEnum::UPDATE);
                
        try{
            $aset = $this->repository->update($request->all(), $id);
        }catch(\Exception $e){
            return $this->errorResponse($e->getMessage());
        }

        return $this->successResponse($aset);
    }
    public function destroy(int $id)
    {
        $this->permissionService->managePermission(ModuleEnum::ASET, Auth::user()->UserRoleId, MasterEnum::DELETE);

        try{
            $aset = $this->repository->find($id);

            $aset->update([
                'MasterAsetDeletedBy' => auth()->user()->UserId
            ]);
            
            $aset->delete();
        }catch(\Exception $e){
            return $this->errorResponse($e->getMessage());
        }

        return $this->successResponse('Data with id '.$id.' has been deleted!');
    }
}
