<?php

namespace App\Http\Controllers;
use App\Enums\MasterEnum;
use App\Enums\ModuleEnum;
use App\Services\EmployeeService;
use App\Services\PermissionService;
use Illuminate\Support\Facades\Auth;
use App\Repositories\EmployeeRepository;
use App\Requests\Employee\CreateEmployeeRequest;
use Illuminate\Http\Request;

class EmployeeController extends Controller
{
    protected $service, 
              $repository,
              $permissionService;
    public function __construct(
        EmployeeService $service, 
        EmployeeRepository $repository,
        PermissionService $permissionService
    )
    {
        $this->service = $service;
        $this->repository = $repository;
        $this->permissionService = $permissionService;
    }
    public function index()
    {
        $limit = request('limit', 10);

        $this->permissionService->managePermission(ModuleEnum::EMPLOYEE, Auth::user()->UserRoleId, MasterEnum::INDEX);

        try {
            $employee = $this->repository->paginate($limit);
        } catch (\Exception $e) {
            return $this->errorResponse($e->getMessage(), $e);
        }
        return $this->successResponse($employee);
    }
    
    public function show(int $id)
    {
        $this->permissionService->managePermission(ModuleEnum::EMPLOYEE, Auth::user()->UserRoleId, MasterEnum::SHOW);

        try{
            $employee = $this->repository->find($id);
        }catch(\Exception $e){
            return $this->errorResponse($e->getMessage(), $e);
        }
        return $this->successResponse($employee);
    }

    public function store(CreateEmployeeRequest $request)
    {
        $this->permissionService->managePermission(ModuleEnum::EMPLOYEE, Auth::user()->UserRoleId, MasterEnum::STORE);

        try{
            $employee = $this->service->store($request->all());
        }catch(\Exception $e){
            return $this->errorResponse($e->getMessage(), $e);
        }
        return $this->successResponse($employee);
    }

    public function update(Request $request, int $id)
    {
        $this->permissionService->managePermission(ModuleEnum::EMPLOYEE, Auth::user()->UserRoleId, MasterEnum::UPDATE);

        try{
            
            $image = $request->file('EmployeeImage') ? $request->file('EmployeeImage') : null;

            $employee = $this->service->update(
                $request->all(),
                $image,
                $id
            );

        }catch(\Exception $e){
            return $this->errorResponse($e->getMessage(), $e);
        }
        return $this->successResponse($employee);
    }

    public function destroy(int $id)
    {
        $this->permissionService->managePermission(ModuleEnum::EMPLOYEE, Auth::user()->UserRoleId, MasterEnum::DELETE);

        try{
            $employee = $this->repository->delete($id);
        }catch(\Exception $e){
        return $this->errorResponse($e->getMessage(), $e);
        }
        return $this->successResponse($employee);
    }
}
