<?php

namespace App\Http\Controllers;

use App\Enums\MasterEnum;
use App\Enums\ModuleEnum;
use Illuminate\Support\Facades\Auth;
use App\Repositories\ModuleRepository;
use App\Requests\Module\CreateModuleRequest;
use App\Requests\Module\UpdateModuleRequest;
use App\Services\PermissionService;
class ModuleController extends Controller
{
    protected $repository, $permissionService;
    public function __construct(
        ModuleRepository $repository,
        PermissionService $permissionService
    )
    {
        $this->repository = $repository;
        $this->permissionService = $permissionService;
    }
    public function index()
    {
        $limit = request('limit', 10);
        
        $this->permissionService->managePermission(ModuleEnum::MODULE, Auth::user()->UserRoleId, MasterEnum::INDEX);
        
        try{
            $modules = $this->repository->paginate($limit);
        }catch(\Exception $e){
            return $this->errorResponse($e->getMessage());
        }
        return $this->successResponse($modules);
    }
    public function show($id)
    {
        $this->permissionService->managePermission(ModuleEnum::MODULE, Auth::user()->UserRoleId, MasterEnum::SHOW);

        try{
            $module = $this->repository->find($id);
        }catch(\Exception $e){
            return $this->errorResponse($e->getMessage());
        }
        return $this->successResponse($module);
    }
    public function update($id, UpdateModuleRequest $request)
    {
        $this->permissionService->managePermission(ModuleEnum::MODULE, Auth::user()->UserRoleId, MasterEnum::UPDATE);

        try{
            $module = $this->repository->update($request->all(), $id);
        }catch(\Exception $e){
            return $this->errorResponse($e->getMessage());
        }
        return $this->successResponse($module);
    }
    public function store(CreateModuleRequest $request)
    {
        $this->permissionService->managePermission(ModuleEnum::MODULE, Auth::user()->UserRoleId, MasterEnum::STORE);

        try{
            $module = $this->repository->create($request->all());
        }catch(\Exception $e){
            return $this->errorResponse($e->getMessage());
        }
        return $this->successResponse($module);
    }
    public function delete($id)
    {
        $this->permissionService->managePermission(ModuleEnum::MODULE, Auth::user()->UserRoleId, MasterEnum::DELETE);
        
        try{
            $module = $this->repository->delete($id);
        }catch(\Exception $e){
            return $this->errorResponse($e->getMessage());
        }
        return $this->successResponse($module);
    }
}
