<?php

namespace App\Http\Controllers;

use App\Enums\MasterEnum;
use App\Enums\ModuleEnum;
use App\Services\PermissionService;
use App\Repositories\RoleRepository;
use Illuminate\Support\Facades\Auth;
use App\Requests\Role\CreateRoleRequest;
use App\Requests\Role\UpdateRoleRequest;

class RoleController extends Controller
{
    protected $repository, $permissionService;
    public function __construct(RoleRepository $repository, PermissionService $permissionService)
    {
        $this->repository = $repository;
        $this->permissionService = $permissionService;
    }
    
    public function index()
    {
        $limit = request('limit', 10);
        
        $this->permissionService->managePermission(ModuleEnum::ROLE, Auth::user()->UserRoleId, MasterEnum::INDEX);

        try{
            $role = $this->repository->paginate($limit);
        }catch(\Exception $e){
            return $this->errorResponse($e->getMessage());
        }
        return $this->successResponse($role);
    }

    public function store(CreateRoleRequest $request)
    {
        $this->permissionService->managePermission(ModuleEnum::ROLE, Auth::user()->UserRoleId, MasterEnum::STORE);
        
        try{
            $role = $this->repository->create($request->all());
        }catch(\Exception $e){
            return $this->errorResponse($e->getMessage());
        }
        return $this->successResponse($role);
    }

    public function show(int $id)
    {
        $this->permissionService->managePermission(ModuleEnum::ROLE, Auth::user()->UserRoleId, MasterEnum::SHOW);

        try{
            $role = $this->repository->find($id);
        }catch(\Exception $e){
            return $this->errorResponse($e->getMessage());
        }
        return $this->successResponse($role);
    }

    public function update(UpdateRoleRequest $request, int $id)
    {
        $this->permissionService->managePermission(ModuleEnum::ROLE, Auth::user()->UserRoleId, MasterEnum::UPDATE);

        try{
            $role = $this->repository->update($request->all(), $id);
        }catch(\Exception $e){
            return $this->errorResponse($e->getMessage());
        }
        return $this->successResponse($role);
    }

    public function destroy(int $id)
    {
        $this->permissionService->managePermission(ModuleEnum::ROLE, Auth::user()->UserRoleId, MasterEnum::DELETE);

        try{
            $role = $this->repository->delete($id);
        }catch(\Exception $e){
            return $this->errorResponse($e->getMessage());
        }
        return $this->successResponse($role);
    }
}
