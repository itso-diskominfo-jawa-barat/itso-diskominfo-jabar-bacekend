<?php

namespace App\Http\Controllers;

use App\Enums\MasterEnum;
use App\Enums\ModuleEnum;
use App\Services\PermissionService;
use App\Repositories\UnitRepository;
use Illuminate\Support\Facades\Auth;
use App\Requests\Unit\CreateUnitRequest;
use App\Requests\Unit\UpdateUnitRequest;

class UnitController extends Controller
{
    protected $repository, $permissionService;
    public function __construct(UnitRepository $repository, PermissionService $permissionService)
    {
        $this->repository = $repository;
        $this->permissionService = $permissionService;
    }
    public function index()
    {
        $limit = request('limit', 10);
        
        $this->permissionService->managePermission(ModuleEnum::UNIT, Auth::user()->UserRoleId, MasterEnum::INDEX);

        try{
            $unit = $this->repository->paginate($limit);
        }catch (\Exception $e){
            $this->errorResponse($e->getMessage());
        }
        return $this->successResponse($unit);
    }
    public function show(int $id)
    {
        $this->permissionService->managePermission(ModuleEnum::UNIT, Auth::user()->UserRoleId, MasterEnum::SHOW);

        try{
            $unit = $this->repository->find($id);
        }catch (\Exception $e){
            $this->errorResponse($e->getMessage());
        }
        return $this->successResponse($unit);
    }

    public function store(CreateUnitRequest $request)
    {
        $this->permissionService->managePermission(ModuleEnum::UNIT, Auth::user()->UserRoleId, MasterEnum::STORE);

        try{
            $unit = $this->repository->create($request->all());
        }catch (\Exception $e){
            $this->errorResponse($e->getMessage());
        }
        return $this->successResponse($unit);
    }

    public function update(UpdateUnitRequest $request, int $id)
    {
        $this->permissionService->managePermission(ModuleEnum::UNIT, Auth::user()->UserRoleId, MasterEnum::UPDATE);

        try{
            $unit = $this->repository->update($request->all(), $id);
        }catch (\Exception $e){
            $this->errorResponse($e->getMessage());
        }
        return $this->successResponse($unit);
    }

    public function destroy(int $id)
    {
        $this->permissionService->managePermission(ModuleEnum::UNIT, Auth::user()->UserRoleId, MasterEnum::DELETE);
        
        try{
           $unit = $this->repository->delete($id);
        }catch (\Exception $e){
            $this->errorResponse($e->getMessage());
        }
        return $this->successResponse($unit);
    }
}
