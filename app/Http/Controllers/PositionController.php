<?php

namespace App\Http\Controllers;

use App\Enums\MasterEnum;
use App\Enums\ModuleEnum;
use App\Services\PermissionService;
use Illuminate\Support\Facades\Auth;
use App\Repositories\PositionRepository;
use App\Requests\Position\CreatePositionRequest;
use App\Requests\Position\UpdatePositionRequest;

class PositionController extends Controller
{

    protected $repository, 
              $permissionService;
    public function __construct(PositionRepository $reposiory, PermissionService $permissionService)
    {
        $this->repository = $reposiory;
        $this->permissionService = $permissionService;
    }

    public function index()
    {
        $limit = request('limit', 10);

        $this->permissionService->managePermission(ModuleEnum::POSITION, Auth::user()->UserRoleId, MasterEnum::INDEX);

        try {
            $employee = $this->repository->paginate($limit);
        } catch (\Exception $e) {
            return $this->errorResponse($e->getMessage(), $e);
        }
        return $this->successResponse($employee);
    }

    public function store( CreatePositionRequest $request)
    {
        $this->permissionService->managePermission(ModuleEnum::POSITION, Auth::user()->UserRoleId, MasterEnum::STORE);

        try{
            $position = $this->repository->create($request->all());
        }catch(\Exception $e){
            return $this->errorResponse($e->getMessage(), $e);
        }
        return $this->successResponse($position);
    }

    public function show(int $id)
    {
        $this->permissionService->managePermission(ModuleEnum::POSITION, Auth::user()->UserRoleId, MasterEnum::SHOW);

        try{
            $position = $this->repository->find($id);
        }catch(\Exception $e){
            return $this->errorResponse($e->getMessage(), $e);
        }
        return $this->successResponse($position);
    }

    public function update(UpdatePositionRequest $request, int $id)
    {
        $this->permissionService->managePermission(ModuleEnum::POSITION, Auth::user()->UserRoleId, MasterEnum::UPDATE);

        try{
            $position = $this->repository->update($request->all(), $id);
        }catch(\Exception $e){
            return $this->errorResponse($e->getMessage(), $e);
        }
        return $this->successResponse($position);
    }
    
    public function destroy(int $id)
    {
        $this->permissionService->managePermission(ModuleEnum::POSITION, Auth::user()->UserRoleId, MasterEnum::DELETE);
        
        try{
            $position = $this->repository->delete($id);
        }catch(\Exception $e){
            return $this->errorResponse($e->getMessage(), $e);
        }
        return $this->successResponse($position);
    }
}
