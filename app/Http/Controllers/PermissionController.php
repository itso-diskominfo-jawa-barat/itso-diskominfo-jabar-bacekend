<?php

namespace App\Http\Controllers;
use App\Enums\MasterEnum;
use App\Enums\ModuleEnum;
use App\Services\PermissionService;
use Illuminate\Support\Facades\Auth;
use App\Repositories\PermissionRepository;
use App\Requests\Permission\CreatePermissionRequest;
use App\Requests\Permission\UpdatePermissionRequest;

class PermissionController extends Controller
{
    protected $repository, $permissionService;
    
    public function __construct(PermissionRepository $repository, PermissionService $permissionService)
    {
        $this->repository = $repository;
        $this->permissionService = $permissionService;
    }

    public function index()
    {
        $limit = request('limit', 10);

        $this->permissionService->managePermission(ModuleEnum::PERMISSION, Auth::user()->UserRoleId, MasterEnum::INDEX);

        try{
            $permission = $this->repository->paginate($limit);
        }catch(\Exception $e){
            return $this->errorResponse($e->getMessage());
        }
        return $this->successResponse($permission);
    }

    public function show(int $id)
    {
        $this->permissionService->managePermission(ModuleEnum::PERMISSION, Auth::user()->UserRoleId, MasterEnum::SHOW);

        try{
            $permission = $this->repository->find($id);
        }catch(\Exception $e){
            return $this->errorResponse($e->getMessage());
        }
        return $this->successResponse($permission);
    }

    public function store(CreatePermissionRequest $request)
    {
        $this->permissionService->managePermission(ModuleEnum::PERMISSION, Auth::user()->UserRoleId, MasterEnum::STORE);

        try{
            $permission = $this->repository->create($request->all());
        }catch(\Exception $e){
            return $this->errorResponse($e->getMessage());
        }
        return $this->successResponse($permission);
    }
    public function update(UpdatePermissionRequest $request, int $id)
    {
        $this->permissionService->managePermission(ModuleEnum::PERMISSION, Auth::user()->UserRoleId, MasterEnum::UPDATE);

        try{
            $permission = $this->repository->update($request->all(), $id);
        }catch(\Exception $e){
            return $this->errorResponse($e->getMessage());
        }
        return $this->successResponse($permission);
    }
    public function destroy(int $id)
    {
        $this->permissionService->managePermission(ModuleEnum::PERMISSION, Auth::user()->UserRoleId, MasterEnum::DELETE);

        try{
            $permission = $this->repository->delete($id);
        }catch(\Exception $e){
            return $this->errorResponse($e->getMessage());
        }
        return $this->successResponse($permission);
    }
}
